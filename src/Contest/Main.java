/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    static boolean isBerhasil = false;
    public static void main(String[] args) {
        //Do your magic here...
        do { //oop do-while akan terus berjalan sampai variabel isBerhasil diatur menjadi true
            try {
                PemesananTiket.run(); //memanggil metode run dari kelas PemesananTiket
                isBerhasil = true; //mengatur variabel isBerhasil menjadi true jika tidak ada pengecualian 
            } catch (InvalidInputException e) { //Jika terjadi pengecualian, kode disamping akan mencetak pesan kesalahan dan melanjutkan loop sampai tidak ada lagi pengecualian 
                System.out.println(e.getMessage());
            }
           
        } while (!isBerhasil);

        //detail pemesanan
        System.out.println("----- Detail Pemesanan -----");
        System.out.println("Nama Pemesan\t\t: "+ PemesananTiket.namaPemesan);
        System.out.println("Kode Booking\t\t: "+ generateKodeBooking());
        System.out.println("Tiket yang dipesan\t: "+ PemesananTiket.dipesan.namaTiket);
        System.out.println("Tanggal Pesanan\t\t: "+ getCurrentDate());
        System.out.println("Total Harga\t\t: "+ PemesananTiket.dipesan.getHarga());
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}