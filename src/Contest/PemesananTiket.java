package Contest;

import java.util.Scanner;

class PemesananTiket {
    // Do your magic here...

    static String namaPemesan;
    static TiketKonser dipesan;
    
    
    public static void run() throws InvalidInputException { //method bernama run dan modifier public static  serta melempar exception dengan tipe  “InvalidInputException”.
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nama pemesan : ");
        namaPemesan = in.nextLine();

        //memastikan bahwa nama pemesan yang dimasukkan oleh pengguna harus kurang dari 10 karakter,  jika tidak maka akan mencetak pesan/message.
        if (namaPemesan.length() >= 10) {
            throw new InvalidInputException("Nama pemesan harus kurang dari 10 karakter");
        }

        //pilihan jenis tiket
        System.out.print("Pilih jenis tiket :");
        System.out.println("\n1. CAT8");
        System.out.println("2. CAT1");
        System.out.println("3. FESTIVAL");
        System.out.println("4. VIP");
        System.out.println("5. VVIP (UNLIMITED EXPERIENCE)");
        System.out.print("masukkan pilihan :");
        int pilihan = in.nextInt();

        //memastikan bahwa pengguna hanya dapat memilih tiket dengan nomor antara 1 hingga 5, jika tidak maka akan mencetak pesan/message.
        if (pilihan < 1 || pilihan > 5) { 
            throw new InvalidInputException("pilihan tiket harus antara 1 hingga 5");
        }

        switch (pilihan) { //pengkondisian dengan 'pilihan' digunakan sebagai kondisi dalam pengkondisian
            case 1:
                dipesan = new CAT8("CAT8", 800000);
                break;
            case 2:
                dipesan = new CAT1("CAT1", 4000000);
                break;
            case 3:
                dipesan = new FESTIVAL("FESTIVAL", 3500000);
                break;
            case 4:
                dipesan = new VIP("VIP", 5000000);
                break;
            case 5:
                dipesan = new VVIP("VVIP (UNLIMITED EXPERIENCE)", 12000000);
                break;
            default: //jika tidak ada nilai yang cocok, maka default akan dieksekusi
                break;
        }
    } 
}